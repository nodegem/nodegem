<div style="text-align: center;">

[![Nodegem.io](https://cdn.nodegem.io/assets/cover.png)](https://www.nodegem.io)

[![Netlify Status](https://api.netlify.com/api/v1/badges/061a935b-5ae2-46d1-a685-a2ef83df940a/deploy-status)](https://app.netlify.com/sites/nodegem/deploys)
[![pipeline status](https://gitlab.com/nodegem/nodegem/badges/master/pipeline.svg)](https://gitlab.com/nodegem/nodegem/commits/master)
[![License: MIT](https://img.shields.io/badge/License-MIT-green.svg)](https://opensource.org/licenses/MIT)

</div>

# About

Nodegem is a visual scripting tool for all sorts of automation projects without the need to know how to program. It is designed to be as inviting as possible to any and all kinds of people varying from no knowledge of programming to expert programmers. The plan is to have all sorts of integrations from many different sources and to be community driven.

# Documentation

For documentation, visit <https://docs.nodegem.io.>

# Quick Deploy

## Nodegem Client

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/Nodegem/nodegem)

[![Deploy](https://cdn.nodegem.io/assets/linode-button.svg)](https://cloud.linode.com/linodes/create?type=One-Click&subtype=Community%20StackScripts&stackScriptID=626196)

## Nodegem WebApi

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://github.com/Nodegem/nodegem)

[![Deploy](https://cdn.nodegem.io/assets/linode-button.svg)](https://cloud.linode.com/linodes/create?type=One-Click&subtype=Community%20StackScripts&stackScriptID=625535)

# Discord

<https://discord.gg/MynGgME>
